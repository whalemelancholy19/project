#include "functions.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <conio.h>
#include <time.h>
#include <QVector>
#include <QDebug>
using namespace std;

void menu()
{
    int **matrix_a = nullptr;
    int  lines_a = 0;
    int columns_a = 0;
    bool inputA = false;

    int **matrix_b = nullptr;
    int  lines_b = 0;
    int columns_b = 0;
    bool inputB = false;

    int **sum = nullptr;
    int **com = nullptr;
    int **sud = nullptr;
    int **num = nullptr;
    int **trans = nullptr;

    int choice = 0;
    int choice2 = 0;
    int numbern = 0;

    bool check = true;


    do
    {
        system("cls");
        cout << "�������� ����� ����:\n";
        cout << "-----------------------\n";
        cout << "1. ���������� ������� �\n";
        cout << "2. ���������� ������� �\n";
        cout << "3. ���������������� �������\n";
        cout << "4. �������� ������ � � �\n";
        cout << "5. ��������� ������ � � �\n";
        cout << "6. ��������� ������ � � �\n";
        cout << "7. �������� ��������� ������ � � �\n";
        cout << "8. ��������� ���� ��������� ������� �� ����� �\n";


        do
        {
            char c = '\0';
            while (scanf("%d%c", &choice, &c, 1) != 2 || c != '\n')
            {
                printf("������� ������ ���� �� ����������. ����������, ���������� �����.\n");
                while (getchar() != '\n');
            }
        } while ((choice < 1) || (choice > 8));

        switch (choice)
        {
            case 1: //���������� ������� �
                system("cls");
                if (inputA == true)
                {
                    for (int i = 0; i < lines_a; i++)
                    {
                        delete[] matrix_a[i];
                    }
                    delete[] matrix_a;
                 }

                cout << "������� ���������� ����� � ������� �: ";
                do
                {
                    char c = '\0';
                    while (scanf("%d%c", &lines_a, &c, 1) != 2 || c != '\n')
                    {
                        cout << "������� ���������� �������� � ������� �:\n";
                        while (getchar() != '\n');
                    }
                } while ((lines_a < 1) || (lines_a > 9));

                cout << "������� ���������� �������� � ������� A: ";
                do
                {
                    char c = '\0';
                    while (scanf("%d%c", &columns_a, &c, 1) != 2 || c != '\n')
                    {
                        cout << "�������� ��������. ����������, ���������� �����.\n";
                        while (getchar() != '\n');
                    }
                } while ((columns_a < 1) || (columns_a > 9));

                matrix_a = addvalues(lines_a, columns_a);
                inputA = true;
                system("pause");
                system("cls");
                break;
            case 2: //���������� ������� B
                system("cls");
                if (inputB == true)
                {
                    for (int i = 0; i < lines_b; i++)
                    {
                        delete[] matrix_b[i];
                    }
                    delete[] matrix_b;
                 }

                cout << "������� ���������� ����� � ������� B: ";
                do
                {
                    char c = '\0';
                    while (scanf("%d%c", &lines_b, &c, 1) != 2 || c != '\n')
                    {
                        cout << "�������� ��������. ����������, ���������� �����.\n";
                        while (getchar() != '\n');
                    }
                } while ((lines_b < 1) || (lines_b > 9));

                cout << "������� ���������� �������� � ������� �: ";
                do
                {
                    char c = '\0';
                    while (scanf("%d%c", &columns_b, &c, 1) != 2 || c != '\n')
                    {
                        cout << "�������� ��������. ����������, ���������� �����.\n";
                        while (getchar() != '\n');
                    }
                } while ((columns_b < 1) || (columns_b > 9));

                matrix_b =  addvalues(lines_b, columns_b);
                inputB = true;
                system("pause");
                system("cls");
                break;
            case 3:
                system("cls");
                cout << "1. ��������������� ������� �\n";
                cout << "2. ��������������� ������� �\n";
                do
                {
                    char c = '\0';
                    while (scanf("%d%c", &choice2, &c, 1) != 2 || c != '\n')
                    {
                        printf("������� ������ ���� �� ����������. ����������, ���������� �����.\n");
                        while (getchar() != '\n');
                    }
                } while ((choice2 < 1) || (choice2 > 2));
                if (choice2 == 1)
                {
                    system("cls");
                    if (inputA == false)
                    {
                        cout << "����������, ������� ��������� �������\n";
                    }
                    else
                    {
                        trans = transposed(matrix_a, lines_a,  columns_a);
                    }
                }
                else
                {
                    system("cls");
                    if (inputB == false)
                    {
                        cout << "����������, ������� ��������� �������\n";
                    }
                    else
                    {
                        trans = transposed(matrix_b, lines_b, columns_b);
                    }
                }
                system("pause");
                system("cls");
                break;
            case 4:
                system("cls");
                if ((inputA == false) || (inputB == false))
                {
                    cout << "����������, ������� ��������� �������\n";
                }
                else
                {
                    sum = summ(matrix_a, lines_a, columns_a, matrix_b, lines_b, columns_b);
                }
                system("pause");
                system("cls");
                break;
            case 5:
                system("cls");
                if ((inputA == false) || (inputB == false))
                {
                    cout << "����������, ������� ��������� �������\n";
                }
                else
                {
                    cout << "1. A-B\n";
                    cout << "2. B-A\n";

                    do
                    {
                        char c = '\0';
                        while (scanf("%d%c", &choice2, &c, 1) != 2 || c != '\n')
                        {
                            printf("������� ������ ���� �� ����������. ����������, ���������� �����.\n");
                            while (getchar() != '\n');
                        }
                    } while ((choice2 < 1) || (choice2 > 2));
                    sud = sudtraction(matrix_a, lines_a,  columns_a,  matrix_b, lines_b, columns_b, choice2);
                }
                system("pause");
                system("cls");
                break;
            case 6:
                system("cls");
                if ((inputA == false) || (inputB == false))
                {
                    cout << "����������, ������� ��������� �������\n";
                }
                else
                {
                    com = composit(matrix_a,  lines_a, columns_a, matrix_b, lines_b, columns_b);
                }
                system("pause");
                system("cls");
                break;
            case 7:
                system("cls");
                if ((inputA == false) || (inputB == false))
                {
                    cout << "����������, ������� ��������� �������\n";
                }
                else
                {
                    check = equality(matrix_a, lines_a, columns_a, matrix_b, lines_b, columns_b);
                    if (check == true)
                    {
                       cout << "������� �����\n";
                    }
                    else
                    {
                       cout << "������� �� �����\n";
                    }
                }
                system("pause");
                system("cls");
                break;
            case 8:
               system("cls");
               cout << "1. �������� �� ����� ������� �\n";
               cout << "2. �������� �� ����� ������� �\n";
               do
               {
                   char c = '\0';
                   while (scanf("%d%c", &choice2, &c, 1) != 2 || c != '\n')
                   {
                       printf("������� ������ ���� �� ����������. ����������, ���������� �����.\n");
                       while (getchar() != '\n');
                   }
               } while ((choice2 < 1) || (choice2 > 2));
               if (choice2 == 1)
               {
                   system("cls");
                   if (inputA == false)
                   {
                       cout << "����������, ������� ��������� �������\n";
                   }
                   else
                   {
                       do
                       {
                            cout << "�� ����� ����� ����� �������� �������?\n";
                            char c = '\0';
                            while (scanf("%d%c", &numbern, &c, 1) != 2 || c != '\n')
                            {
                                cout << "�������� ��������. ����������, ���������� �����.\n";
                                while (getchar() != '\n');
                            }
                       } while ((numbern < -100) || (numbern > 100));
                       cout << "\n";
                       num = number(matrix_a, lines_a, columns_a, numbern);
                   }
               }
               else
               {
                   system("cls");
                   if (inputB == false)
                   {
                       cout << "����������, ������� ��������� �������\n";
                   }
                   else
                   {
                       do
                       {
                            cout << "�� ����� ����� ����� �������� �������?\n";
                            char c = '\0';
                            while (scanf("%d%c", &numbern, &c, 1) != 2 || c != '\n')
                            {
                                cout << "�������� ��������. ����������, ���������� �����.\n";
                                while (getchar() != '\n');
                            }
                       } while ((numbern < -100) || (numbern > 100));
                       cout << "\n";
                       num = number(matrix_b, lines_b, columns_b, numbern);
                   }
               }
               system("pause");
               system("cls");
                break;
        }
    } while(choice != 9);

    for (int i = 0; i < lines_a ; i++)
    {
        delete[] matrix_a[i];
    }
    delete[] matrix_a;

    for (int i = 0; i < lines_b ; i++)
    {
        delete[] matrix_b[i];
    }
    delete[] matrix_b;
}
