#include "functions.h"
#include <iostream>
#include <QDebug>

using namespace std;

int** summ(int** matrix_a, int  lines_a, int columns_a, int** matrix_b, int  lines_b, int columns_b)
{
    int** sum = nullptr;

    if ( lines_a != lines_b && columns_a != columns_b)
    {
        cout << "�������� ����������, �.�. ������ ������ �� ���������!\n";
    }
    else
    {
        cout <<"�������� ������� A: \n";
        for (int i = 0; i < lines_a; i++)
        {
            for (int j = 0; j < columns_a; j++)
            {
                cout << matrix_a[i][j]<<"\t";
            }
            cout << "\n";
        }

        cout <<"�������� ������� B: \n";
        for (int i = 0; i < lines_b; i++)
        {
            for (int j = 0; j < columns_b; j++)
            {
                cout << matrix_b[i][j]<<"\t";
            }
            cout << "\n";
        }

        cout << "������� �����: \n";
        sum = new int* [lines_a];
        for (int i = 0; i < lines_a; i++)
        {
            sum[i] = new int[columns_a];
        }

        for (int i = 0; i < lines_a; i++)
        {
            for (int j = 0; j < columns_a; j++)
            {
                sum[i][j] = matrix_a[i][j] + matrix_b[i][j];
                cout << sum[i][j]<<"\t";
            }
            cout << "\n";
        }
    }
 cout << "\n\n\n";
 return sum;
}
