#ifndef TST_TEST_PROJECT_H
#define TST_TEST_PROJECT_H

#include <QtCore>
#include <QtTest/QtTest>

class test_project : public QObject
{
    Q_OBJECT

public:
    test_project();

private slots:
        void test_equality();
        void test_transposed();
        void test_summ();
        void test_sudtraction();
        void test_composit();
        void test_number();
};

#endif // TST_TEST_PROJECT_H
