#include "functions.h"
#include <iostream>
#include <QDebug>

using namespace std;

int** composit(int** matrix_a, int  lines_a, int columns_a, int** matrix_b, int  lines_b, int columns_b)
{
    int** com = nullptr;
    if(columns_a != lines_b)
    {
        cout << "��� ��������� ����������, ����� ���������� �������� \n ������ ������� ���� ����� ���������� ����� ������\n";
    }
    else
    {
        cout << "�������� ������� A: \n";
        for (int i = 0; i < lines_a; i++)
        {
            for (int j = 0; j < columns_a; j++)
            {
                cout << matrix_a[i][j]<<"\t";
            }
            cout << "\n";
        }

        cout <<"�������� ������� B: \n";
        for (int i = 0; i < lines_b; i++)
        {
            for (int j = 0; j < columns_b; j++)
            {
                cout << matrix_b[i][j]<<"\t";
            }
            cout << "\n";
        }

        cout<< "������� ������������: \n";
        com = new int* [lines_a];
        for (int i = 0; i < lines_a; i++)
        {
            com[i] = new int[columns_b];
            for (int j = 0; j<columns_b; j++)
            {
                com[i][j] = 0;
                for (int k = 0; k < columns_a; k++)
                {
                    com[i][j] += matrix_a[i][k] * matrix_b[k][j];
                }
            }
        }

        for (int i = 0; i < lines_a; i++)
        {
            for (int j = 0; j < columns_b; j++)
            {
                cout << com[i][j]<<"\t";
            }
            cout << "\n";
        }
    }
 cout << "\n\n\n";
 return com;
}
