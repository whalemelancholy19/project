#include <QCoreApplication>
#include <locale.h>
#include <windows.h>
#include "functions.h"
#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    setlocale(LC_ALL, "Rus");
    menu();
    return a.exec();
}
